from matplotlib import pyplot

# X and Y values of the points in the plot:
xs = [1, 2, 3, 4, 5]
ys = [2, 4, 6, 8, 10]

# .plot() creates a plot of given parameters. It can take optional argument 
# (in a form used by MATLAB: https://matplotlib.org/2.0.2/api/pyplot_api.html)
# to format color and style of lines or points to draw:
pyplot.plot(xs, ys, "g-o")

# .show() displays the plot in a new window
pyplot.show()

# Multiple graphs in the same window, v1:
xs = [0, 1, 2, 3, 4]
y1 = [1, 2, 3, 4, 5]
y2 = [1, 2, 4, 8, 16]

pyplot.plot(xs, y1, xs, y2)
pyplot.show()

# Multiple graphs in the same window, v2 (for readibility when making many plots):
pyplot.plot(xs, y1)
pyplot.plot(xs, y2)
pyplot.show()