from matplotlib import pyplot
import numpy

array = numpy.arange(1, 21).reshape(5, 4)
# this array looks like this:
# [[ 1  2  3  4]
#  [ 5  6  7  8]
#  [ 9 10 11 12]
#  [13 14 15 16]
#  [17 18 19 20]]

# passing a 2-d array plots each column of the array as the y-values
# for a graph:
pyplot.plot(array, "g-o")
pyplot.show()