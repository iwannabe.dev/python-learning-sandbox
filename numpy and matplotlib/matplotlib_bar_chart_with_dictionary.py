from matplotlib import pyplot

fruits = {
    "apples": 10,
    "oranges": 16,
    "bananas": 9,
    "pears": 4,
}

pyplot.bar(fruits.keys(), fruits.values())
pyplot.show()

