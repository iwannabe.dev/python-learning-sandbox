from matplotlib import pyplot
import numpy

days = numpy.arange(0, 21)
other_site = numpy.arange(0, 21)
real_python = other_site ** 2

pyplot.plot(days, other_site)
pyplot.plot(days, real_python)

# x-axis ticks
pyplot.xticks([0, 5, 10, 15, 20])

# x-axis label
pyplot.xlabel("Days of Reading")

# y-axis label
pyplot.ylabel("Amount of Python Learned")

# Plot title
pyplot.title("Python Learned Reading Real Python vs Other Site")

# Legend (https://matplotlib.org/2.0.2/users/legend_guide.html)
pyplot.legend(["Other Site", "Real Python"])

pyplot.show()
