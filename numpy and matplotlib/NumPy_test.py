import numpy

# Traditional 2-d array (matrix in form of "list of lists"):
print("Traditional 2-d array (matrix in form of 'list of lists'):")
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print(matrix, "\n")

# Multiplying every element of array by 2:
print("Multiplying every element of array by 2:")
for row in matrix:
    for num in range(len(row)):
        row[num] = row[num] * 2
print(matrix, "\n")

##############################################################################

# NumPy approach to create 2-d array:
print("NumPy approach to create 2-d array:")
numpy_matrix = numpy.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
print(numpy_matrix, "\n")

# Accessing a single element of matrix is the same as when using list:
print("Accessing a single element of matrix is the same as when using list:")
print(numpy_matrix[0][2], "\n")

# or it can be accessed by providing a row and column index in a single
# square bracets:
print("Accessing a single element of matrix by providing a row and column:")
print(numpy_matrix[0, 2])

# Multiplying every element of array by 2:
print("Multiplying every element of array by 2:")
print(2 * numpy_matrix, "\n")

# Performing element-wise arithmetic (+, -, *, /) on multidimensional arrays:
print("Performing element-wise arithmetic on multidimensional arrays:")
second_matrix = numpy.array([[5, 4, 3], [7, 6, 5], [9, 8, 7]])
print(second_matrix - numpy_matrix, "\n")

# Matrix product (pol. mnożenie macieży) z użyciem znaku @:
print("Matrix product (pol. mnożenie macieży) z użyciem znaku @:")
matrix1 = numpy.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
print(matrix1 @ matrix1)
