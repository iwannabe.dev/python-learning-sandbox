class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def insert_at_beginning(self, data):
        """Insert a new node at the beginning of the list."""
        new_node = Node(data)
        new_node.next = self.head
        self.head = new_node

    def insert_at_end(self, data):
        """Add a new node at the end of the list."""
        if self.head is None:
            self.insert_at_beginning(data)
            return

        new_node = Node(data)
        last_node = self.head
        while last_node.next:
            last_node = last_node.next

        last_node.next = new_node

    def insert_at_position(self, data, key):
        """Insert a new node after a specified key (where the key is the value)."""
        if self.head is None:
            self.insert_at_beginning(data)
            return

        new_node = Node(data)

        if self.head.data == key:
            new_node.next = self.head.next
            self.head.next = new_node
            return

        current_node = self.head
        while current_node and current_node.data != key:
            current_node = current_node.next
        if current_node:
            new_node.next = current_node.next
            current_node.next = new_node

    def delete_from_beginning(self):
        """Remove a node from the beginning of the list."""
        if self.head is None:
            return

        temp = self.head.next
        self.head = temp

    def delete_from_end(self):
        """Remove a node from the end of the list."""
        if self.head is None:
            return

        if self.head.next is None:
            self.head = None
            return

        next_to_last = None
        last_node = self.head
        while last_node.next:
            next_to_last = last_node
            last_node = last_node.next

        if next_to_last:
            next_to_last.next = None

    def delete_node(self, key):
        """Remove a node containing specified data."""
        if self.head is None:
            return

        if self.head.data == key and self.head.next is not None:
            self.head = self.head.next
            return

        if self.head.data == key:
            self.head = None
            return

        previous_item = self.head
        current_item = self.head.next
        while current_item:
            if current_item.data == key:
                previous_item.next = current_item.next
                return
            previous_item = current_item
            current_item = current_item.next

    def search(self, key):
        """Find a node containing data, return a reference to the node or None."""
        result = None
        if self.head.data == key:
            result = self.head.data
        else:
            next_item = self.head.next
            while next_item:
                if next_item.data == key:
                    result = next_item.data
                next_item = next_item.next

        return result

    def display(self):
        """Display all elements of the list."""
        if self.head:
            print(self.head.data)
            next_item = self.head.next
            while next_item:
                print(next_item.data)
                next_item = next_item.next

    def clear(self):
        """Remove all nodes from the list, clear the list."""
        self.head = None

    def size(self):
        """Return the number of nodes in the list."""
        counter = 0
        if self.head:
            counter += 1
            next_item = self.head.next
            while next_item:
                counter += 1
                next_item = next_item.next
        return counter


if __name__ == "__main__":
    ll = LinkedList()
    ll.insert_at_beginning(1)
    ll.insert_at_beginning(2)
    ll.insert_at_beginning(3)
    ll.insert_at_beginning(4)
    ll.insert_at_beginning(5)
    ll.display()

    print("Found: ", ll.search(3))