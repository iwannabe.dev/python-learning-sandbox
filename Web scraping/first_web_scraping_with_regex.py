from urllib.request import urlopen
import re

url = "http://olympus.realpython.org/profiles/dionysus"
html_page = urlopen(url)
html_text = html_page.read().decode("utf-8")

pattern = "<title.*?>.*?</title.*?>"
match_results = re.search(pattern, html_text, re.IGNORECASE)
title = match_results.group()
print(title)
title_no_tags = re.sub("<.*?>", "", title)
print(title_no_tags)