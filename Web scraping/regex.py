import re # module for regex

## method re.findall() returns a list of all matches. If no matches are found,
## an empty list is returned.
print(".findall() method:")

# asterisk sign * (zero or more)
result = re.findall("ab*c", "ac")  # result -> ['ac']
print(result)

# dot sign . (any single character)
result = re.findall("a.c", "abcac")  # result -> ['abc']
print(result)

# dot and asterisk sign .* (any character or characters repeated zero lub more times)
result = re.findall("a.*c", "abbbbc")  # result -> ['abbbbc']
print(result)

##############################################################################

## method re.search() - returns an object calld a MatchObject that stores 
# different "groups" of data. This is because there might be matches inside of 
# other matches, and re.search() returns every possible result, but we can get 
# the first and most inclusive result by calling a .group() method on a 
# MatchObject object.
print("\n.search() method:")

march_results = re.search("ab*c", "ABC", re.IGNORECASE)
print(march_results.group()) # result -> 'ABC'

##############################################################################

## method re.sub() - short for "substitute" - allows to replace text in a 
# string that matches a regular expression with new text.
print("\n.sub() method:")

string = "Everything is <replaced> if it's in <tags>."

# using "greedy" regular expression - longest possible match using *
result = re.sub("<.*>", "ELEPHANTS", string)
print(result) # result -> "Everything is ELEPHANTS"

# using "non-greedy" regular expression - shortest possible match using *?
result2 = re.sub("<.*?>", "ELEPHANTS", string)
print(result2) # result -> "Everything is ELEPHANTS if it's in ELEPHANTS."

##############################################################################

print("\nOther examples:")
# ignore case sensitivity
result = re.findall("ab*c", "AC", re.IGNORECASE)  # result -> ['AC']
print(result)