from bs4 import BeautifulSoup
from urllib.request import urlopen

url = "http://olympus.realpython.org/profiles/dionysus"

page = urlopen(url)
html = page.read().decode("utf-8")
soup = BeautifulSoup(html, "html.parser")

# Get all text from the document and remove any HTML tags:
print(soup.get_text())

# Find all URLs for all images on the page:
print(soup.find_all("img")) # -> [<img src="/static/dionysus.jpg"/>, <img src="/static/grapes.png"/>]

# .find_all() method does, in fact, return a Tag object, not a string, which 
# contains some useful info. Ie. a 'name' attribute contains a HTML tag type:
image1, image2 = soup.find_all("img")
print(image1.name) # -> 'img' 
print(image2.name) # -> 'img' 

# To access HTML attributes of the Tag object pass an attribute name to the Tag
# object in a square brackets (like when passing a key to a dictionary):
print(image1["src"]) # -> '/static/dionysus.jpg'
print(image2["src"]) # -> '/static/grapes.png'

# Certain tags in HTML document can be accessed by properties of the Tag object, ie:
print(soup.title) # -> '<title>Profile: Dionysus</title>'

# Accessing a string from a HTML tag:
print(soup.title.string) # -> 'Profile: Dionysus'