import mechanicalsoup

# Create an instance of a Browser class and assign HTML content to login_html:
browser = mechanicalsoup.Browser()
url = "http://olympus.realpython.org/login"
login_page = browser.get(url)
login_html = login_page.soup

# Retrive all <form> elements from HTML code and fill out the form.
# .select() returns a list of all requested elements and we can access them 
# by their index number. This HTML has only 1 form, hence we call 0th element:
form = login_html.select("form")[0]
# Select the username and password inputs and ser their values to "zeus" and
# "ThunderDude", respectively:
form.select("input")[0]["value"] = "zeus"
form.select("input")[1]["value"] = "ThunderDude"

# Submit the form by passing the form variable and the URL of the login_page:
profiles_page = browser.submit(form, login_page.url)

# Print a URL that browser has been redirected to after successfully submitting a form:
print(profiles_page.url)

# Launch a browser and display HTML code of 'profiles_page':
browser.launch_browser(profiles_page.soup)

################################

# Get all relative URLs ('href' attributes) from the profiles_page and print them:
links = profiles_page.soup.select("a")
for link in links:
    address = link["href"]
    text = link.text
    print(f"{text}: {address}")