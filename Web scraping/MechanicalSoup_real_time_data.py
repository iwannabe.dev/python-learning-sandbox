import mechanicalsoup
import time

browser = mechanicalsoup.Browser()
url = "http://olympus.realpython.org/dice"

# Retrive 4 result from the website in 3s intervals between checks:
for i in range(4):
    page = browser.get(url)
    tag = page.soup.select("#result")[0]
    result = tag.text
    print(f"The result of your dice roll is: {result}")
    
    if i < 3:
        time.sleep(3)
