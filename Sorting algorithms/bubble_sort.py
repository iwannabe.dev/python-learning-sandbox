import random

def generate_random_numbers(no_of_elements, pool):
    return random.sample(range(pool), no_of_elements)

def bubble_sort(numbers):
    n = len(numbers)
    
    for i in range(n):
        is_sorted = True

        for j in range(n-1-i):
            if numbers[j] > numbers[j+1]:
                numbers[j], numbers[j+1] = numbers[j+1], numbers[j]
                is_sorted = False
        
        if is_sorted:
            break

    return numbers
                          

numbers = generate_random_numbers(100, 10000)
print(f"Nieposortowane: {numbers} \n")
print(f"Posortowane: {bubble_sort(numbers)}")