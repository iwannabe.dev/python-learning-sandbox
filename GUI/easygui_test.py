import  easygui


# .msgbox() - A dialog box for displaying a message with a single button. It 
# returns the label of the button.
# Default button label is 'OK', but we can change it by passing third
# parameter to msgbox() -> 'ok_button = "New name for button"'
def msgbox_test():
    button_label = easygui.msgbox(
        msg = "Hello!", 
        title = "My first message box", 
        ok_button = "Click me"
    )
    print(button_label)


# buttonbox() - A dialog box with several buttons. It returns the label of the
# selected button.
def buttonbox():
    choice = easygui.buttonbox(
        msg = "What's your favourite colour?",
        title = "Choose wisely...",
        choices = ("Red", "Yellow", "Blue")
    )
    print(choice)


# indexbox() - A dialog box with several buttons. It returns the index of the
# selected button.
def indexbox():
    colors = ("Red", "Yellow", "Blue")
    choice = easygui.indexbox(
        msg = "What's your favourite colour?",
        title = "Choose wisely...",
        choices = colors
    )
    print(colors[choice])


# enterbox() - A dialog box with a text entry box. It returns the text entered
def enterbox():
    message_entered = easygui.enterbox(
        msg = "What's your favourite colour?",
        title = "Favourite colour"
    )
    print(message_entered)


# fileopenbox() - A dialog box for selecting a file to be opened. It returns
# the absolute path to the selected file.
def fileopenbox():
    file_path = easygui.fileopenbox(msg="Select a file")
    if file_path is None:
        exit()
    else:
        print(file_path)


# diropenbox() - A dialog box for selecting a directory to be opened. It 
# returns the absolute path to the selected directory.
def diropenbox():
    dir_path = easygui.diropenbox(msg="Select a dir")
    print(dir_path)

# filesavebox() - A dialog box for saving a file. It returns the absolute path
# to the location for saving the file.
def filesavebox():
    file_path = easygui.filesavebox(msg="Save a file")
    print(file_path)


if __name__ == '__main__':
    # msgbox_test()
    # buttonbox()
    # indexbox()
    # enterbox()
    fileopenbox()
    # diropenbox()
    # filesavebox()
