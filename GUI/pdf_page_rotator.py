import easygui
from PyPDF2 import PdfFileReader, PdfFileWriter


def source_path():
    path = easygui.fileopenbox(
        msg = "Select a PDF file",
        default = "*.pdf"
    )

    if path is None:
        exit()
    else:
        return path


def destination_path():
    path = easygui.filesavebox(
        msg = "Save a file",
        default = "*.pdf"
    )
    if path is None:
        exit()
    else:
        return path

    
def rotation_angle():
    # degree sign: \N{DEGREE SIGN}
    angles = ("90", "180", "270")
    angle = easygui.indexbox(
        msg = "Select an angle you want to rotate PDF's pages by:",
        title = "Rotation angle",
        choices = angles
    )
    return int(angles[angle])


def dialog_box(message):
    easygui.msgbox(
        msg = message, 
        title = "Error message"
    )


def rotate_pdf(source_file_path, destination_file_path, angle):
    pdf_source_file = PdfFileReader(source_file_path)
    new_pdf = PdfFileWriter()

    for page in pdf_source_file.pages:
        page = page.rotateClockwise(angle)
        new_pdf.addPage(page)

    with open(destination_file_path, "wb") as output_file:
        new_pdf.write(output_file)

if __name__ == '__main__':
    source_file_path = source_path()
    angle = rotation_angle()
    destination_file_path = destination_path()

    while source_file_path == destination_file_path:
        dialog_box("Error! Change the filename or directory - you can't override source file.")
        destination_file_path = destination_path()
    
    rotate_pdf(source_file_path, destination_file_path, angle)