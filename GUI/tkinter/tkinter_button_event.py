import tkinter as tk


def decrease():
    """Get a value of 'text' attribute of Label widget with a dictionary-style 
    subscript notation and decrease it by 1"""
    value = int(label_value["text"])
    label_value["text"] = f"{value - 1}"


def increase():
    """Get a value of 'text' attribute of Label widget with a dictionary-style 
    subscript notation and increase it by 1"""
    value = int(label_value["text"])
    label_value["text"] = f"{value + 1}"


window = tk.Tk()
window.rowconfigure(index=0, minsize=50, weight=1)
window.columnconfigure(index=[0, 1, 2], minsize=50, weight=1)

# Using button's attribute 'command' as an alternative to .bind() method to
# assign an event handler. It works only with Button widget! 
button_decrease = tk.Button(master=window, text="-", command=decrease)
button_decrease.grid(row=0, column=0, sticky="nsew")

label_value = tk.Label(master=window, text="0")
label_value.grid(row=0, column=1)

button_increase = tk.Button(master=window, text="+", command=increase)
button_increase.grid(row=0, column=2, sticky="nsew")


window.mainloop()
