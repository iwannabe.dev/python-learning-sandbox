import tkinter as tk


# .place() method is another geometry manager that can precisely control
# location of a widget within a window or frame to 1 pixel precision. It
# takes 2 arguments: .place(x=10, y=15) that represents position of top
# left corner of a widget (counting from top-left corner of a window).
# Layout made with .place() can be difficult to manage with a lot of widgets
# and is not responsive when window is resized.


def place():
    window = tk.Tk()
    frame = tk.Frame(master=window, width=200, height=200)
    frame.pack()

    label1 = tk.Label(master=frame, text="I'm at (0, 0)", bg="red")
    label1.place(x=0, y=0)

    label2 = tk.Label(master=frame, text="I'm at (75, 75)", bg="yellow")
    label2.place(x=75, y=75)

    window.mainloop()


def main():
    place()

if __name__ == '__main__':
    main()