import tkinter as tk

#
# # Label widget is used to display text or image.
# window = tk.Tk()
# label = tk.Label(
#     text="Siema",
#     foreground="white", # set the text color to white
#     # fg="white", # Alternative version of above
#     background="#34A2FE", # set the background color to given hex RGB value
#     # bg="#34A2FE", # Alternative version of above
#     width=50, # set width of window in 'text units' (size of digit '0' in OS' default font)
#     height=23, # set height of window in 'text units' (size of digit '0' in OS' default font)
# )
# label.pack()
# window.mainloop()
#
# # .destroy() closes the window
# # window.destroy()
#
#
# ##############################################################################
# # Button widget displays a clickable button
# window2 = tk.Tk()
# button = tk.Button(
#     text="Click me!",
#     width=17,
#     height=5,
#     bg="blue",
#     fg="yellow"
# )
# button.pack()
# window2.mainloop()
#
#
# ##############################################################################
# # Entry widget displays a text field with a sigle line of text
# window3 = tk.Tk()
# entry = tk.Entry(
#     fg="yellow",
#     bg="pink",
#     width=30,
# )
# entry.pack()
#
# # .get() method retrives the text
# user_input = entry.get()
#
# # .delete() method deletes character at a given index or characters of a given
# # range, ie. entry.delete(4,8) deletes characters at index 4 to 7 (it works
# # like a string slicing, so includes first, but excludes the last character).
# entry.delete(0)
# entry.delete(0, 4)
# entry.delete(0, tk.END) # removes all text in Entry
#
# # .insert() inserts a text into an Entry widget
# entry.insert(0, "Python")
#
# window3.mainloop()
#
#
# ##############################################################################
# # Text widget displays a multiline text field.
# window4 = tk.Tk()
# text_box = tk.Text()
# text_box.pack()
#
# # .get() works different here. It take a string as parameter in a form of
# # "line.char", where 'line' starts from 1 and 'char' starts from 0
# text_box.get("1,0") # get first character (index 0) from line 1
# text_box.get("1,0", "1.5") # get first 5 characters (index 0-4) from line 1
# text_box.get("1.0", tk.END) # get ALL text from 'text_box'
#
# # .delete() also takes a string representing position of acharacter(s)
# text_box.delete("1.0") # delete first character from line 1
# text_box.delete("1.0", "1.4") # delete first 4 characters from line 1
# text_box.delete("1.0", tk.END) # delete ALL text from 'text_box'
#
# # .insert() also uses "line.character" string as parameter
# text_box.insert("1.0", "Hello") # insert text "hello" at index 0 on line 1
# text_box.insert(tk.END, "\nPut me at the end!") # insert text at the end of
#                                                 # text_box in new line
# window4.mainloop()

##############################################################################
# Frame widget displays a rectangular - it's used to group related widgets or
# provide padding between widgets
window5 = tk.Tk()

frame_a = tk.Frame()
label_a = tk.Label(master=frame_a, text="I'm in Frame A")
label_a.pack()

# Adding a border to frame, by passing a border type to relief parameter
# ('tk.FLAT', 'tk.SUNKEN', 'tk.RAISED', 'tk.GROOVE' or 'tk.RIDGE') and
# setting a size of border with 'border=6' parameter
frame_b = tk.Frame(master=window5, relief=tk.GROOVE, border=6)
label_b = tk.Label(master=frame_b, text="I'm in Frame B")

# Setting direction of a widget with 'side=' parameter
label_b.pack(side=tk.LEFT) # doesn't make sense in this particular example

# Order of 'frame_a' and 'frame_b' is swapped
frame_b.pack()
frame_a.pack()

window5.mainloop()

