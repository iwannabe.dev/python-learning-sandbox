import tkinter as tk


# .grid() method is yet another geometry manager that is likely used most
# often. It provides all power of .pack() in an easier way. .grid() works
# by splitting a window or frame into rows and columns.


def grid_test():
    window = tk.Tk()

    for i in range(3):
        for j in range(3):
            frame = tk.Frame(
                master=window,
                relief=tk.RAISED,
                borderwidth=1
            )
            frame.grid(row=i, column=j)
            label = tk.Label(master=frame, text=f"Row {i}\nColumn {j}")
            label.pack()

    window.mainloop()



# External padding adds some space around the outside of a grid cell. It's
# controlled with two keyword arguments of .grid(): 'padx' and 'pady':
def grid_padding():
    window = tk.Tk()

    for i in range(3):
        for j in range(3):
            frame = tk.Frame(
                master=window,
                relief=tk.RAISED,
                borderwidth=1
            )
            frame.grid(row=i, column=j, padx=5, pady=5) # external padding
            label = tk.Label(master=frame, text=f"Row {i}\nColumn {j}")
            label.pack(padx=5, pady=5) # internal padding

    window.mainloop()


def grid_responsive_resizing():
    window = tk.Tk()

    for i in range(3):
        # both .columnconfigure() and .rowconfigure() take 3 arguments:
        # 1. index of the grid column or row to be configured (it can take
        # a list too to configure multiple elements), 2. 'weight=' parameter
        # that determines how the column/row should respond to window resizing
        # relative to the other columns/rows, 3. 'minsize=' that sets minimum
        # size in pixels.
        window.columnconfigure(i, weight=1, minsize=85)
        window.rowconfigure(i, weight=1, minsize=50)

        for j in range(3):
            frame = tk.Frame(
                master=window,
                relief=tk.RAISED,
                borderwidth=1
            )
            frame.grid(row=i, column=j, padx=5, pady=5) # external padding
            label = tk.Label(master=frame, text=f"Row {i}\nColumn {j}")
            label.pack(padx=5, pady=5) # internal padding

    window.mainloop()


# By default widgets are centered in their grid cells:
def grid_centering_widgets():
    window = tk.Tk()
    window.columnconfigure(index=0, minsize=250)
    window.rowconfigure(index=[0, 1], minsize=100)

    label1 = tk.Label(text="A")
    label1.grid(row=0, column=0)

    label2 = tk.Label(text="B")
    label2.grid(row=1, column=0)

    window.mainloop()


# 'sticky=' parameter of the .grid() method can be used to change the location
# of each label inside the grid cell. 'sticky=' parameter accepts initials
# (lower or upper case) of cardinal directions ("N", "S", "E", "W") and we can
# combine them too.
def grid_widgets_position_within_cell():
    window = tk.Tk()
    window.columnconfigure(index=0, minsize=250)
    window.rowconfigure(index=[0, 1], minsize=100)

    label1 = tk.Label(text="A")
    label1.grid(row=0, column=0, sticky="n") # stick to "north" (top center)

    label2 = tk.Label(text="B")
    label2.grid(row=1, column=0, sticky="ne") # stick to "north-east" (top-right)

    window.mainloop()


# Filling the grid using 'sticky=' parameter. It works similar to .pack()'s
# 'fill=' parameter ( ie.: ".grid(sticky="nsew")" = ".pack(fill=tk.BOTH)" ).
def grid_filling():
    window = tk.Tk()
    window.columnconfigure(index=[0, 1, 2, 3], minsize=50)
    window.rowconfigure(index=0, minsize=50)

    label1 = tk.Label(text="1", bg="black", fg="white")
    label1.grid(row=0, column=0)  # centered (by default)

    label2 = tk.Label(text="2", bg="black", fg="white")
    label2.grid(row=0, column=1, sticky="ew") # fill horizontally

    label3 = tk.Label(text="3", bg="black", fg="white")
    label3.grid(row=0, column=2, sticky="ns")  # fill vertically

    label4 = tk.Label(text="4", bg="black", fg="white")
    label4.grid(row=0, column=3, sticky="nsew")  # fill entire cell

    window.mainloop()

def main():
    grid_filling()

if __name__ == '__main__':
    main()