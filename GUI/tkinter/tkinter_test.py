import tkinter as tk

# Create window which is an instance of a Tk class
window = tk.Tk()

# Label widget - class Label lets add some text
greeting = tk.Label(text="Hello, Tkinter")

# Pack a widget - to add a widget we use, ie., a .pack() method
greeting.pack()

window.mainloop()
