import tkinter as tk


def convert_fahrenheit_to_celsius(temperature_degree_f):
    temp = float(temperature_degree_f)
    return (5/9 * (temp - 32))


def update_result():
    converted_temperature = convert_fahrenheit_to_celsius(textbox.get())
    label_result["text"] = f"{converted_temperature:.1f}"


window = tk.Tk()
window.title("Temperature Converter")
window.columnconfigure(index=[0, 1, 2, 3, 4], weight=1)
window.rowconfigure(index=0, weight=1, minsize=40)

textbox = tk.Entry(master=window, width=10)
textbox.grid(row=0, column=0)

# Label displaying temperature unit - fixed text
label_degree_F = tk.Label(master=window, text="°F")
label_degree_F.grid(row=0, column=1, sticky="w")

button = tk.Button(master=window, text="\N{RIGHTWARDS BLACK ARROW}", command=update_result)
button.grid(row=0, column=2)

# Label displaying conversion result
label_result = tk.Label(master=window, text="0")
label_result.grid(row=0, column=3)

# Label displaying temperature unit - fixed text
label_degree_C = tk.Label(master=window, text="°C")
label_degree_C.grid(row=0, column=4)

window.mainloop()