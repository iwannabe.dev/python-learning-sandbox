import tkinter as tk

# Tkinter's .mainloop() automatically maintains a list of events that have 
# occured and runs an event handler any time a new event is added to that list.
# Simplified code of what .mainloop() does would be something like:
#
# #Assume that this list gets updated automatically
# events_list = []
#
# #Run the event loop
# while True:
#     if events_list == []:
#         continue
#
#     # If execution reaches this pount, then there is at least one event
#     # objest in events_list
#     event = events_list[0]
#
#     # If event is a keypress event object
#     if event.type == "keypress":
#         # Call the keypress event handler
#         handle_keypress(event)


###############################################################################
# Example 1:

# Event handler that prints out character associated with a key being pressed
def handle_keypress(event):
    """Print the character asociated to the key pressed"""
    print(event.char)


def keypress_example():
    window = tk.Tk()

    # Bind event to event handler using .bind() method. .bind() is bound to the
    # widget on which is called. It always takes 2 arguments: 
    # 1. An event name in form of string "<event_name>". List of all event 
    #    names: http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/event-types.html
    # 2. A name of event handler to be called whenever event occurs. 
    # When the event handler is called, the event object is passed to the event
    # handler function.
    window.bind("<Key>", handle_keypress)

    window.mainloop()


###############################################################################
# Example 2:

def handle_click(event):
    print("The button was clicked")


def button_click_example():
    window = tk.Tk()

    button = tk.Button(master=window, text="Click me!")
    button.pack()

    # Binding a "<Button-1>" event to the handle_click() event handler function
    button.bind("<Button-1>", handle_click)

    window.mainloop()


###############################################################################

def main():
    button_click_example()

if __name__ == '__main__':
    main()