import tkinter as tk


# 1.
# .pack() method with fixed frame size
def pack_fixed_frame():
    window = tk.Tk()
    frame1 = tk.Frame(master=window, width=100, height=100, bg="red")
    frame1.pack()

    frame2 = tk.Frame(master=window, width=50, height=50, bg="yellow")
    frame2.pack()

    frame3 = tk.Frame(master=window, width=25, height=25, bg="blue")
    frame3.pack()

    window.mainloop()


# 2.
# .pack() method with a variable frame size using 'fill=' parameter.
# 'fill=tk.X' fills horizontally, 'fill=tk.Y' fills vertically and
# 'fill=tk.BOTH' fills in both directions.
def pack_variable_frame():
    window = tk.Tk()
    frame1 = tk.Frame(master=window, width=100, height=100, bg="red")
    frame1.pack(fill=tk.X)

    frame2 = tk.Frame(master=window, height=50, bg="yellow")
    frame2.pack(fill=tk.X)

    frame3 = tk.Frame(master=window, height=25, bg="blue")
    frame3.pack(fill=tk.X)

    window.mainloop()


# 3.
# .pack() method with a 'side=' parameter to place frames at specified position
# Available values of 'side=': 'tk.TOP', 'tk.BOTTOM', 'tk.LEFT', 'tk.RIGHT'
def pack_side_parameter():
    window = tk.Tk()
    frame1 = tk.Frame(master=window, width=200, height=100, bg="red")
    frame1.pack(fill=tk.Y, side=tk.LEFT)

    frame2 = tk.Frame(master=window, width=100, bg="yellow")
    frame2.pack(fill=tk.Y, side=tk.LEFT)

    frame3 = tk.Frame(master=window, width=50, bg="blue")
    frame3.pack(fill=tk.Y, side=tk.LEFT)

    window.mainloop()


# 4.
# .pack() method with responsive window resizing
def pack_responsive_resize():
    window = tk.Tk()
    frame1 = tk.Frame(master=window, width=200, height=100, bg="red")
    frame1.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)

    frame2 = tk.Frame(master=window, width=100, bg="yellow")
    frame2.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)

    frame3 = tk.Frame(master=window, width=50, bg="blue")
    frame3.pack(fill=tk.BOTH, side=tk.LEFT, expand=True)

    window.mainloop()

def main():
    # pack_fixed_frame()
    # pack_variable_frame()
    # pack_side_parameter()
    pack_responsive_resize()

if __name__ == '__main__':
    main()