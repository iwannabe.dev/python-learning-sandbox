# small script to decrypt a .pdf file (password protected)

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

# location of source PDF
pdf_path = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "newsletter_protected.pdf"
)

pdf_reader = PdfFileReader(str(pdf_path))
pdf_reader.decrypt(password="SuperSecret")


pdf_writer = PdfFileWriter()

# adding all pages from existing .pdf
pdf_writer.appendPagesFromReader(pdf_reader)

with Path("newsletter_unprotected.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)
