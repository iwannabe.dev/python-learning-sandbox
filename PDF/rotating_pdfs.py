# Simple code to rotate every odd page of .pdf file

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

pdf_path = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "ugly.pdf"
)

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

# loop over all pages
for n in range(pdf_reader.getNumPages()):
    page = pdf_reader.getPage(n)
    # rotate only pages of odd indices (which are even page numbers)
    if n % 2 == 0:
        page.rotateClockwise(90)
    pdf_writer.addPage(page)

with Path("ugly_rotated.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)
