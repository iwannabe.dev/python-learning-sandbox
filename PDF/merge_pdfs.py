# Simple code to merge multiple pdf files (stored in one folder) into one file.

from PyPDF2 import PdfFileMerger
from pathlib import Path

pdf_merger = PdfFileMerger()

reports_dir = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "expense_reports"
)

expense_reports = list(reports_dir.glob("*.pdf"))
expense_reports.sort()

for path in expense_reports:
    pdf_merger.append(str(path))

with Path("expense_reports.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)
