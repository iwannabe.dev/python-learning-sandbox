# Simple code to merge multiple pdf files (stored in one folder) into one file.
# v2 using .merge() method

from PyPDF2 import PdfFileMerger
from pathlib import Path

report_dir = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "quarterly_report"
)

report_path = report_dir / "report.pdf"
toc_path = report_dir / "toc.pdf"

pdf_merger = PdfFileMerger()
pdf_merger.append(str(report_path))

pdf_merger.merge(1, str(toc_path))

with Path(report_dir / "full_report.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)
