# small script to encrypt a .pdf file with password

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

# location of source PDF
pdf_path = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "newsletter.pdf"
)

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

# adding all pages from existing .pdf
pdf_writer.appendPagesFromReader(pdf_reader)

# set a user password (owner_pwd is an optional argument which,
# by default, is set to the same value as user_pwd argument
pdf_writer.encrypt(user_pwd="SuperSecret")

with Path("newsletter_protected.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)
