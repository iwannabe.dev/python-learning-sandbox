# small script to export multiple pages from one .pdf file
# and save it as another .pdf

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

# location of source PDF
pdf_path = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "Pride_and_Prejudice.pdf"
)

input_pdf = PdfFileReader(str(pdf_path))

pdf_writer = PdfFileWriter()

# extracting pages at index 1, 2 and 3
for n in range(1,4):
    page = input_pdf.getPage(n)
    pdf_writer.addPage(page)

with Path("chapter1.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)


