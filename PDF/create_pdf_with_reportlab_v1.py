from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import A4

# instance of Canvas (atribute 'pagesize' is optional)
canvas = Canvas("hello.pdf", pagesize=A4)

# setting a font (it's optional)
canvas.setFont("Times-Roman", 18)

# writing a string
canvas.drawString(1 * inch, 6 * inch, "Hello World")\

# saving canvas to pdf file
canvas.save()
