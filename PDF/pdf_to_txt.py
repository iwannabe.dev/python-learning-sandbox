# small application extracting text from a .pdf file
# and saving it to a .txt file. All using PyPDF2 module

from pathlib import Path
from PyPDF2 import PdfFileReader

# location of source PDF
pdf_path = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "Pride_and_Prejudice.pdf"
)

pdf_reader = PdfFileReader(str(pdf_path))
output_file_path = Path.home() / "Pride and Prejudice.txt"

with output_file_path.open(mode="w") as output_file:
    output_file.write(
        f"{pdf_reader.documentInfo.title}\n"
        f"Number of pages: {pdf_reader.getNumPages()}\n\n"
    )

    for page in pdf_reader.pages:
        text = page.extractText()
        output_file.write(text)
