# small script to copy all pages from one .pdf file
# and save it as another .pdf

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

# location of source PDF
pdf_path = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "Pride_and_Prejudice.pdf"
)

input_pdf = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()
pdf_writer.appendPagesFromReader(input_pdf)

with Path("full_copy_of_pdf.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)


