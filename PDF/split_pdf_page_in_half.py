# small script to a .pdf file in half and save it as another .pdf

from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter
import copy

# location of source PDF
pdf_path = (
    Path.home() /
    "Za raczke" /
    "python-learning-sandbox" /
    "PDF" /
    "half_and_half.pdf"
)

pdf_reader = PdfFileReader(str(pdf_path))
first_page = pdf_reader.getPage(0)

# working on copy of page to prevent changes and allow to reuse original page
left_side = copy.deepcopy(first_page)

# getting current coordinates of the upper right corner
current_coords = left_side.mediaBox.upperRight

# setting new coordinates to crop left side of the page
new_coords = (current_coords[0] / 2, current_coords[1])
left_side.mediaBox.upperRight = new_coords

right_side = copy.deepcopy(first_page)
right_side.mediaBox.upperLeft = new_coords

pdf_writer = PdfFileWriter()
pdf_writer.addPage(left_side)
pdf_writer.addPage(right_side)

with Path("cropped_pages_in_half.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)

    
